/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   GPMConfig.hpp
 * Author: znix
 *
 * Created on 6 August 2016, 1:51 PM
 */

#pragma once
#include <boost/filesystem.hpp>
#include <string>
#include <vector>
#include <map>
#include "json/json.h"
#include "Package.hpp"
#include "networking.hpp"
namespace fs = boost::filesystem;
using namespace std;

namespace GPM {
	class GPMConfig {
	public:
		GPMConfig(fs::path cachedir, fs::path datadir);
		virtual ~GPMConfig();

		void checkRepos(bool = true);
		void refreshRepos();
		void readRepos();
		void addRepo(Repo*);

		void setProgressFunc(ProgressFunc progressFunc) {
			this->progressFunc = progressFunc;
		}

		bool isPackageSandboxMode() const {
			return packageSandboxMode;
		}

		void setPackageSandboxMode(bool packageSandboxMode) {
			this->packageSandboxMode = packageSandboxMode;
		}
	private:
		void clearNameLookup();
		void SaveInstalledPackages();

		vector<Repo*> repos;
		map<string, Package*> nameLookup;
		fs::path cachedir;
		fs::path datadir;
		Json::Value packagesInstalled;
		ProgressFunc progressFunc;
		bool packageSandboxMode;

		friend class PackageManager;
		friend class Package;
	};
}

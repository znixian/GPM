/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   PackageManager.hpp
 * Author: znix
 *
 * Created on 6 August 2016, 5:43 PM
 */

#pragma once
#include <string>

#include <boost/filesystem/path.hpp>
#include "GPMConfig.hpp"
#include "DownloadManager.hpp"

namespace GPM {
	namespace Callbacks {
		typedef void (*DownloadPackage)(Package* package, DownloadManager* downloader);
		typedef void (*InstallPackage)(Package* package, DownloadManager* downloader);
		typedef void (*RemovePackage)(Package* package);

		typedef bool (*PreInstallPackages)(list<Package*>& packages);
		typedef bool (*PreRemovePackages)(list<Package*>& packages);
	}

	class PackageManager {
	public:
		PackageManager(GPMConfig* config);
		~PackageManager();

		/**
		 * Installs the given packages.
		 * @param packageNames The names of the packages to install.
		 */
		void Install(vector<string>& packageNames, Callbacks::PreInstallPackages confirmation,
				Callbacks::DownloadPackage downloader, Callbacks::InstallPackage installer);
		void Remove(vector<string>& packageNames, Callbacks::PreRemovePackages confirmation,
				Callbacks::RemovePackage remover);
		void ShowDependencies(string package, ostream* out);
		void ShowAntiDependencies(string package, ostream* out);
		bool IsPackageInstalled(string package);
		void Search(string term, vector<string>& matches);
	private:
		GPMConfig* config;
		void Download(Package* pkg, Callbacks::DownloadPackage package);
		void Unpack(Package* pkg, Callbacks::InstallPackage package);
		void Delete(Package* pkg, Callbacks::RemovePackage callback);
		bool IsPackageInstalled(Package* package);
		void IterateDeps(Package* pkg, ostream* out, list<Package*>* installList = NULL,
				string prefix = " ", string cha = "", bool wasLast = false);
		void IterateRems(Package* pkg, ostream* out, list<Package*>* removeList = NULL,
				string prefix = " ", string cha = "", bool wasLast = false);

		DownloadManager downloadManager;
	};
}

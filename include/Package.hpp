/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   Package.hpp
 * Author: znix
 *
 * Created on 6 August 2016, 1:57 PM
 */

#pragma once
#include <string>
#include <vector>
#include "json/json.h"
#include "GPMConfig.hpp"
using namespace std;

namespace GPM {

	enum AliasType {
		none, soft, hard
	};

	class Package {
	public:
		Package(Json::Value tag, string name, GPMConfig* parent,
				AliasType aliasType = none);
		~Package();
		void SetupDependencies();

		vector<Package*>* GetDependencies();

		string GetName() const {
			return name;
		}

		Package* GetAlias();

		AliasType GetAliasType() const {
			return aliasType;
		}

		Json::Value& GetDataTag();
	private:
		string name;
		vector<Package*>* dependencies;
		Json::Value tag;
		GPMConfig* parent;
		Package* alias;
		AliasType aliasType;
	};
}

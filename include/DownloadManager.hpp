/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   DownloadManager.hpp
 * Author: znix
 *
 * Created on 19 August 2016, 4:08 PM
 */

#pragma once
#include <boost/filesystem.hpp>
#include <string>
#include "networking.hpp"
namespace fs = boost::filesystem;
using namespace std;

namespace GPM {

	class DownloadManager {
	public:
		DownloadManager(fs::path cachedir);
		virtual ~DownloadManager();

		fs::path Download(string url, ProgressFunc progress, bool cacheResult = true);
		fs::path GetPath(string url);
	private:
		fs::path cachedir;
	};
}

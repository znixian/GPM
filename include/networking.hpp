/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   networking.hpp
 * Author: Campbell Suter
 *
 * Created on 6 August 2016, 12:23 PM
 */

#pragma once
#include <string>
#include <boost/filesystem/path.hpp>
using namespace std;
namespace fs = boost::filesystem;

namespace GPM {
	typedef int (*ProgressFunc)(double dltotal, double dlnow);
	string sha256(string input);

	class Downloader {
	public:
		Downloader();
		void perform();

		void setProgress(ProgressFunc progress) {
			this->progress = progress;
		}

		void setSaveTo(fs::path saveTo) {
			this->saveTo = saveTo;
		}

		void setUrl(string url) {
			this->url = url;
		}
	private:
		string url;
		fs::path saveTo;
		ProgressFunc progress;
	};
}

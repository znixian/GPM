#pragma once
#include <string>
using namespace std;

namespace GPM {
	class Repo;
	class GPMConfig;
	class Package;
	class PackageManager;
	class DownloadManager;
}

#include "Exceptions.hpp" // kinda different from the rest

#include "GPMConfig.hpp"
#include "Repo.hpp"
#include "Package.hpp"
#include "PackageManager.hpp"
#include "DownloadManager.hpp"

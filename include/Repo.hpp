/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   Repo.hpp
 * Author: znix
 *
 * Created on 6 August 2016, 1:54 PM
 */

#pragma once

namespace GPM {

	class Repo {
	public:
		Repo(string);
		virtual ~Repo();

		const string GetUrl() const {
			return url;
		}

		const string GetHash() const {
			return hash;
		}

		friend ostream& operator<<(ostream& os, const Repo& obj);
	private:
		const string url;
		const string hash;
	};
}

#include "include/GPM.hpp"
#include "include/networking.hpp"
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <iostream>
#include <string>
#include "include/json/json.h"

#define PACKAGES_FILE "packages.conf"

namespace fs = boost::filesystem;
using namespace std;

namespace GPM {

	GPMConfig::GPMConfig(fs::path path, fs::path data) : cachedir(path),
	datadir(data), progressFunc(NULL), packageSandboxMode(false) {
		if (!fs::is_directory(cachedir)) {
			fs::create_directories(cachedir);
		}
		if (!fs::is_directory(datadir)) {
			fs::create_directories(datadir);
		}

		fs::path packagesFile = datadir / PACKAGES_FILE;

		if (fs::exists(packagesFile)) {
			fs::ifstream in(packagesFile);
			Json::Reader reader;
			bool b = reader.parse(in, packagesInstalled);
			in.close();

			if (!b) {
				throw JsonException(reader.getFormattedErrorMessages(), packagesFile.generic_string());
			}
		} else {
			packagesInstalled = Json::Value(Json::ValueType::objectValue);
		}
	}

	GPMConfig::~GPMConfig() {
		for (Repo* repo : repos) {
			delete repo;
		}
		clearNameLookup();
	}

	void GPMConfig::checkRepos(bool autoRefresh) {
		for (Repo* repo : repos) {
			// the file the cache is stored in.
			fs::path cachefile = cachedir / repo->GetHash();

			// does a cached version exist
			bool exists = fs::exists(cachefile);

			// TTL of a cached copy
			time_t cacheLife = 10; // 10 sec

			// if the TTL expires, mark it so it thinks the cache doesn't exist,
			// so it downloads it again.
			if (exists && fs::last_write_time(cachefile) < time(NULL) - cacheLife) {
				// tell it to download it again.
				exists = false;
			}

			// if the cache file does not exist, download it
			if (!exists) {
				Downloader dl;

				// set the URL and destination.
				dl.setUrl(repo->GetUrl());
				dl.setSaveTo(cachefile);

				// function to call with progress reports.
				dl.setProgress(progressFunc);

				// run the downloader.
				dl.perform();
			}
		}
		readRepos();
	}

	void GPMConfig::refreshRepos() {
		fs::remove_all(cachedir);
		fs::create_directories(cachedir);
		checkRepos();
	}

	void GPMConfig::readRepos() {
		clearNameLookup();
		for (Repo* repo : repos) {
			fs::path cachefile = cachedir / repo->GetHash();
			if (fs::exists(cachefile)) {
				fs::ifstream in(cachefile);
				Json::Value root;
				Json::Reader reader;
				bool b = reader.parse(in, root);
				in.close();
				if (!b) {
					// syntax error
					throw JsonException(reader.getFormattedErrorMessages(), cachefile.generic_string());
				}
				Json::Value& packages = root["packages"];
				for (Json::Value& package : packages) {
					string name = package["name"].asString();
					if (package.isMember("alias")) {
						bool soft = package["alias"]["type"].asString() == "soft";
						AliasType type = soft ? AliasType::soft : AliasType::hard;
						nameLookup[name] = new Package(package, name, this, type);
					} else {
						nameLookup[name] = new Package(package, name, this);
					}
				}
			}
		}
	}

	void GPMConfig::addRepo(Repo* repo) {
		repos.push_back(repo);
	}

	void GPMConfig::clearNameLookup() {
		for (pair<string, Package*> package : nameLookup) {
			delete package.second;
		}
		nameLookup.clear();
	}

	void GPMConfig::SaveInstalledPackages() {
		if (packageSandboxMode) return;
		fs::path packagesFile = datadir / PACKAGES_FILE;
		fs::ofstream myfile(packagesFile);
		myfile << packagesInstalled;
		myfile.close();
	}

}

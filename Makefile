CPPFLAGS=-g -Wall -std=gnu++11
LDFLAGS=-g
LDLIBS=-lboost_system -lboost_filesystem -lcrypto -lcurl
.PHONY: clean

OBJDIR=build/
BINDIR=bin/

LIBGPM=$(BINDIR)libgpm.so
TESTS=$(BINDIR)tests

SRCS=GPMConfig.cpp Repo.cpp jsoncpp.cpp networking.cpp Package.cpp \
    PackageManager.cpp DownloadManager.cpp Exceptions.cpp
OBJS=$(subst .cpp,.o,$(addprefix $(OBJDIR),$(SRCS)) )
HEADS=$(addprefix include/,GPM.hpp)

all: $(TESTS)

$(OBJDIR)%.o: %.cpp $(HEADS)
	g++ -fPIC $(CPPFLAGS) -c $< -o $@

$(LIBGPM): $(OBJS)
	g++ -shared $(LDFLAGS) -o $(LIBGPM) $(OBJS) $(LDLIBS)

$(TESTS): $(LIBGPM) tests.cpp
	g++ $(LDFLAGS) $(CPPFLAGS) -o $(TESTS) tests.cpp -L$(BINDIR) -lgpm $(LDLIBS)

clean:
	mkdir -p $(OBJDIR)
	mkdir -p $(BINDIR)
	rm -f $(OBJS) $(LIBGPM) $(TESTS)


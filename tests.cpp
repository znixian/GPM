#include <iostream>
#include <string>
#include "include/GPM.hpp"
#include "include/networking.hpp"
using namespace std;
using namespace GPM;

int progress(double dltotal, double dlnow) {
	//	int percent = (dlnow / dltotal * 100);
	//	cout << "\r" << percent << "%";
	return 0;
}

bool confInstall(list<Package*>& packages) {
	cout << "About to Download:" << endl;
	for (Package* package : packages) {
		cout << " * " << package->GetName() << endl;
	}
	return true;
}

bool confRemove(list<Package*>& packages) {
	cout << "About to Remove:" << endl;
	for (Package* package : packages) {
		cout << " * " << package->GetName() << endl;
	}
	return true;
}

void dl(Package* package, DownloadManager* downloader) {
	//	cout << "Downloading " << package->GetName() << endl;
}

void install(Package* package, DownloadManager* downloader) {
	//	cout << "Installing " << package->GetName() << endl;
}

void remove(Package* package) {
	//	cout << "Installing " << package->GetName() << endl;
}

int main() {
	try {
		GPMConfig* manager = new GPMConfig("cache/", "data");
		manager->addRepo(new Repo("localhost/~znix/gpm/repotest.1"));
		manager->setPackageSandboxMode(true);
		manager->setProgressFunc(&progress);

		cout << "Checking repos..." << endl;
		manager->checkRepos();

		PackageManager* pkg = new PackageManager(manager);

		vector<string> packages;
		packages.push_back("pk1");
		pkg->ShowDependencies("pk1", &cout);
		pkg->Install(packages, confInstall, dl, install);

		packages.clear();
		packages.push_back("pk2");
		pkg->Remove(packages, confRemove, remove);

		cout << "Searching for @" << endl;

		vector<string> results;
		pkg->Search("@", results);
		for (string pkg : results) {
			cout << " - " << pkg << endl;
		}

		delete pkg;
		delete manager;
	} catch (JsonException ex) {
		cout << ex.GetFile() << ": " << ex.GetMessage() << endl;
	}
}

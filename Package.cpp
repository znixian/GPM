/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "include/GPM.hpp"
#include "include/json/json.h"
#include <iostream>
#include <string>
using namespace std;

#define DEPS "dependencies"
#define ALIAS "alias"
#define ALIAS_TARGET "target"
#define DATA "data"

namespace GPM {

	Package::Package(Json::Value tag, string name, GPMConfig* parent,
			AliasType aliasType)
	: name(name), dependencies(NULL), tag(tag), parent(parent),
	alias(NULL), aliasType(aliasType) {
	}

	Package::~Package() {
		if (dependencies != NULL) {
			delete dependencies;
		}
	}

	vector<Package*>* Package::GetDependencies() {
		if (dependencies == NULL) {
			dependencies = new vector<Package*>();
			if (tag.isMember(DEPS)) {
				for (Json::Value dep : tag[DEPS]) {
					Package* other = parent->nameLookup[dep.asString()];
					other->GetDependencies();
					dependencies->push_back(other->GetAlias());
				}
			}
		}
		return dependencies;
	}

	Package* Package::GetAlias() {
		if (aliasType == none) {
			return this;
		}
		if (alias == NULL) {
			Package* other = parent->nameLookup[tag[ALIAS][ALIAS_TARGET].asString()];
			alias = other->GetAlias();
		}
		return alias;
	}

	Json::Value& Package::GetDataTag() {
		return tag[DATA];
	}

}

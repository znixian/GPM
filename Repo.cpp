#include "include/GPM.hpp"
#include <iostream>
#include <string>
#include "include/networking.hpp"
using namespace std;

namespace GPM {

	Repo::Repo(string url) : url(url), hash(sha256(url)) {
	}

	Repo::~Repo() {
	}

	ostream& operator<<(ostream& os, const Repo& obj) {
		os << obj.url;
		return os;
	}
}

/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "include/GPM.hpp"
#include <string>
#include <iostream>
#include <algorithm>
#include <list>
#include <regex>
using namespace std;

namespace GPM {

	PackageManager::PackageManager(GPMConfig* config) : config(config),
	downloadManager(config->cachedir) {
	}

	PackageManager::~PackageManager() {
	}

	void PackageManager::ShowDependencies(string packageName, ostream* out) {
		// find the target package
		Package* pkg = config->nameLookup[packageName];

		// throw if is does not exist
		if (pkg == NULL) throw NoSuchPackageException(packageName);

		// fix aliases
		pkg = pkg->GetAlias();

		// print the deps into the stream.
		IterateDeps(pkg, out);
	}

	void PackageManager::ShowAntiDependencies(string packageName, ostream* out) {
		// find the target package
		Package* pkg = config->nameLookup[packageName];

		// throw if is does not exist
		if (pkg == NULL) throw NoSuchPackageException(packageName);

		// fix aliases
		pkg = pkg->GetAlias();

		// print the deps into the stream.
		IterateRems(pkg, out);
	}

	void PackageManager::Install(vector<string>& packageNames,
			Callbacks::PreInstallPackages confirmation,
			Callbacks::DownloadPackage downloader, Callbacks::InstallPackage installer) {
		// List of packages to install
		vector<Package*> packages;

		// for each name, find it and add it to the list of packages
		for (string packageName : packageNames) {
			Package* pkg = config->nameLookup[packageName];
			// if the package does not exist, raise a exception.
			if (pkg == NULL) throw NoSuchPackageException(packageName);

			// chase down aliases
			pkg = pkg->GetAlias();

			// if it is already installed, do nothing.
			if (IsPackageInstalled(pkg)) continue;

			// add it to the todo list.
			packages.push_back(pkg);
		}

		// The final list of packages, including dependencies.
		list<Package*> packagesIncDeps;

		// for each target package,
		for (Package* package : packages) {
			// find the dependencies for it, and add them.
			IterateDeps(package, NULL, &packagesIncDeps);
		}

		// confirm with the program using the API.
		if (!confirmation(packagesIncDeps)) return;

		// for each package to be installed, download and unpack it.
		for (Package* pack : packagesIncDeps) {
			Download(pack, downloader);
			Unpack(pack, installer);
		}
	}

	void PackageManager::Remove(vector<string>& packageNames,
			Callbacks::PreRemovePackages confirmation,
			Callbacks::RemovePackage remover) {
		// List of packages to install
		vector<Package*> packages;

		// for each name, find it and add it to the list of packages
		for (string packageName : packageNames) {
			Package* pkg = config->nameLookup[packageName];
			// if the package does not exist, raise a exception.
			if (pkg == NULL) throw NoSuchPackageException(packageName);

			// chase down aliases
			pkg = pkg->GetAlias();

			// if it is already installed, do nothing.
			if (!IsPackageInstalled(pkg)) continue;

			// add it to the todo list.
			packages.push_back(pkg);
		}

		// The final list of packages, including dependencies.
		list<Package*> packagesIncDeps;

		// for each target package,
		for (Package* package : packages) {
			// find the dependencies for it, and add them.
			IterateRems(package, NULL, &packagesIncDeps);
		}

		// confirm with the program using the API.
		if (!confirmation(packagesIncDeps)) return;

		// for each package to be installed, download and unpack it.
		for (Package* package : packagesIncDeps) {
			Delete(package, remover);
		}
	}

	void PackageManager::Download(Package* pkg, Callbacks::DownloadPackage callback) {
		if (config->packageSandboxMode) return;
		callback(pkg, &downloadManager);
	}

	void PackageManager::Unpack(Package* pkg, Callbacks::InstallPackage callback) {
		if (!config->packageSandboxMode) {
			// unpack it
			callback(pkg, &downloadManager);
		}

		// add in a tag to the installed packages list
		config->packagesInstalled[pkg->GetName()] =
				Json::Value(Json::ValueType::objectValue);

		Json::Value& tag = config->packagesInstalled[pkg->GetName()];
		tag["supports"] = Json::Value(Json::ValueType::objectValue);

		for (Package* dep : *(pkg->GetDependencies())) {
			Json::Value& tag2 = config->packagesInstalled[dep->GetName()];
			tag2["supports"][pkg->GetName()] = Json::Value(true);
		}

		// Save the list. Might be a bit inefficent doing this over and over,
		// but is should be fast enough and prevents packages being installed but
		// not marked as such, and getting stuck in limboland.
		config->SaveInstalledPackages();
	}

	void PackageManager::Delete(Package* pkg, Callbacks::RemovePackage callback) {
		if (!config->packageSandboxMode) {
			// unpack it
			callback(pkg);
		}

		// add in a tag to the installed packages list
		config->packagesInstalled.removeMember(pkg->GetName());

		// Save the list. Might be a bit inefficent doing this over and over,
		// but is should be fast enough and prevents packages being installed but
		// not marked as such, and getting stuck in limboland.
		config->SaveInstalledPackages();
	}

	bool PackageManager::IsPackageInstalled(Package* package) {
		return IsPackageInstalled(package->GetName());
	}

	bool PackageManager::IsPackageInstalled(string package) {
		return config->packagesInstalled.isMember(package);
	}

	void PackageManager::IterateDeps(Package* pkg, ostream* out, list<Package*>* installList,
			string prefix, string cha, bool wasLast) {
		// is the package already installed?
		bool isInstalled = IsPackageInstalled(pkg);

		if (installList == NULL) {
			if (isInstalled) {
				(*out) << prefix << cha << "\x1b[36m" << pkg->GetName() << "\x1b[0m" << endl;
			} else {
				(*out) << prefix << cha << pkg->GetName() << endl;
			}
		} else {
			// if this package is not already installed, add it to the todo list.
			if (!isInstalled) {
				// move it to the front, so the packages have
				// their deps when they install.
				installList->remove(pkg);
				installList->push_front(pkg);
			}
		}

		// call IterateDeps on this package's dependencies, too.
		// not needed if it is already installed.
		if (!isInstalled) {
			vector<GPM::Package*>* vec = pkg->GetDependencies();
			for (std::vector<GPM::Package*>::iterator it = vec->begin(); it != vec->end(); ++it) {
				// is this the last item (ie, would the next item stop the for loop)
				bool last = it + 1 == vec->end();

				// prefix to be used for the next lot of deps. The prefix this item
				// is using, plus:
				// * if there was not a character specific to this item, leave the
				//     prefix alone. So, the first items just use the original
				//	   prefix, plus the item-specific character.
				// * else, use a pipe if this was not the last item.
				string prefix2 = prefix + (cha.empty() ? "" : (wasLast ? " " : "┃"));

				// print out this dependency, and all it's dependencies, etc.
				// if it is the last item, use a branching pipe, otherwise use a
				// t-junction one.
				IterateDeps(*it, out, installList, prefix2, last ? "┖" : "┠", last);
			}
		}
	}

	void PackageManager::IterateRems(Package* pkg, ostream* out, list<Package*>* removeList,
			string prefix, string cha, bool wasLast) {
		// is the package already installed?
		if (!IsPackageInstalled(pkg)) return;

		if (removeList == NULL) {
			(*out) << prefix << cha << pkg->GetName() << endl;
		} else {
			// move it to the front, so the packages have
			// their deps when they install.
			removeList->remove(pkg);
			removeList->push_front(pkg);
		}

		// call IterateDeps on this package's reverse dependencies, too.
		vector<GPM::Package*> vec;

		Json::Value& tag2 = config->packagesInstalled[pkg->GetName()];
		Json::Value::iterator ite = tag2["supports"].begin();
		Json::Value::iterator end = tag2["supports"].end();
		while (ite != end) {
			Package* user = config->nameLookup[ite.name()];
			vec.push_back(user);
			ite++;
		}

		for (std::vector<GPM::Package*>::iterator it = vec.begin(); it != vec.end(); ++it) {
			// is this the last item (ie, would the next item stop the for loop)
			bool last = it + 1 == vec.end();

			// prefix to be used for the next lot of deps. The prefix this item
			// is using, plus:
			// * if there was not a character specific to this item, leave the
			//     prefix alone. So, the first items just use the original
			//	   prefix, plus the item-specific character.
			// * else, use a pipe if this was not the last item.
			string prefix2 = prefix + (cha.empty() ? "" : (wasLast ? " " : "┃"));

			// print out this dependency, and all it's dependencies, etc.
			// if it is the last item, use a branching pipe, otherwise use a
			// t-junction one.
			IterateRems(*it, out, removeList, prefix2, last ? "┖" : "┠", last);
		}
	}

	void PackageManager::Search(string term, vector<string>& matches) {
		regex match(term);

		for (pair<string, Package*> items : config->nameLookup) {
			if (regex_search(items.second->GetName(), match)) {
				matches.push_back(items.second->GetName());
			}
		}
	}

}

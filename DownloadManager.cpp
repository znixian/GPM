/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "include/GPM.hpp"
#include "include/DownloadManager.hpp"

namespace GPM {

	DownloadManager::DownloadManager(fs::path cachedir) : cachedir(cachedir) {
	}

	DownloadManager::~DownloadManager() {
	}

	fs::path DownloadManager::Download(string url, ProgressFunc progress, bool useCache) {
		// where should it save
		fs::path path = GetPath(url);

		// is it cached
		if (fs::exists(path) && useCache) {
			// cached it, just reuse it.
			return path;
		}

		// download it
		Downloader dl;
		dl.setProgress(progress);
		dl.setSaveTo(path);
		dl.setUrl(url);
		dl.perform();

		return path;
	}

	fs::path DownloadManager::GetPath(string url) {
		return cachedir / sha256(url);
	}

}

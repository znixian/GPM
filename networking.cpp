/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "include/networking.hpp"
#include <string>
#include <curl/curl.h>
#include "openssl/sha.h"
using namespace std;

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
	size_t written = fwrite(ptr, size, nmemb, (FILE *) stream);
	return written;
}

namespace GPM {

	Downloader::Downloader() {

	}

	string sha256(string input) {
		const int outLen = 65;
		char outputBuffer[outLen];
		unsigned char hash[SHA256_DIGEST_LENGTH];
		const char *str = input.c_str();
		SHA256_CTX sha256;
		SHA256_Init(&sha256);
		SHA256_Update(&sha256, str, strlen(str));
		SHA256_Final(hash, &sha256);
		int i = 0;
		for (i = 0; i < SHA256_DIGEST_LENGTH; i++) {
			size_t offset = (i * 2);
			char* start = outputBuffer + offset;
			size_t lenMax = outLen - offset;
			snprintf(start, lenMax, "%02x", hash[i]);
		}
		outputBuffer[64] = 0;
		return outputBuffer;
	}

	void Downloader::perform() {
		CURL *curl_handle;
		FILE *pagefile;

		curl_global_init(CURL_GLOBAL_ALL);

		/* init the curl session */
		curl_handle = curl_easy_init();

		/* set URL to get here */
		curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());

		/* Switch on full protocol/debug output while testing */
		curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 0L);

		/* disable progress meter, set to 0L to enable and disable debug output */
		curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0L);

		/* send all data to this function  */
		curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

		curl_easy_setopt(curl_handle, CURLOPT_XFERINFOFUNCTION, progress);
		curl_easy_setopt(curl_handle, CURLOPT_XFERINFODATA, this);

		/* open the file */
		pagefile = fopen(saveTo.c_str(), "wb");
		if (pagefile) {

			/* write the page body to this file handle */
			curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);

			/* get it! */
			curl_easy_perform(curl_handle);

			/* close the header file */
			fclose(pagefile);
		}

		/* cleanup curl stuff */
		curl_easy_cleanup(curl_handle);
	}
}
